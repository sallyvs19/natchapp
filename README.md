# README #

CSc 59866/59867 Capstone I/II Fall 2014-Spring 2015

CS/CpE/EE/Psy Joint Senior Design Program 

Smart Living and Assistive Technologies for People in Need 

### What is this repository for? ###

This repository is for NATCH's reading application. You can find more information about us here: 

http://ccvcl.org/wiki/Capstone_2014-2015:NFCPublic

We are currently in Version Super Alpha. 


### How do I get set up? ###
TBA

### Contribution guidelines ###
This project is private and not open to contribution.

### Who do I talk to? ###

Sally Ventura sally0919@gmail.com

Anmol Jaitley jaitley9@gmail.com

Furqan Kahn furqankhank2@gmail.com